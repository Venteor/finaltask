Project name: Handkr

Description: The application is a generator of handkerchiefs - the special cards with people's initials. 
After a user created a handkerchief, he can browse them in the Gallery section, and share via email or save to Photos.